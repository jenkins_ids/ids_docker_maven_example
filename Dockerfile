FROM tomcat:7-jre8

# tomcat-users.xml define los usuarios para la interfaz grafica de Tomcat
ADD tomcat/tomcat-users.xml $CATALINA_HOME/conf/

# ADD tomcat/catalina.sh $CATALINA_HOME/bin/
ADD tomcat/run.sh $CATALINA_HOME/bin/run.sh
RUN chmod +x $CATALINA_HOME/bin/run.sh

# Creamos la carpeta para la aplicación
RUN mkdir $CATALINA_HOME/webapps/ExampleWebApp

ENV JPDA_ADDRESS="8000"
ENV JPDA_TRANSPORT="dt_socket"

# Iniciamos Tomcat en el puerto para debug
EXPOSE 8080
CMD ["run.sh"]